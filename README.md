## Installation Laravel
If your computer have installed to step 1.3
## 1.1 Install Docker on Ubuntu 16.04

```
sudo apt-get install apt-transport-https ca-certificates
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo systemctl enable docker
sudo usermod -aG docker ${USER}
sudo reboot
  ```

## 1.2 Install Docker-compose

  - Install using python-pip: 

```
sudo apt-get -y install python-pip
sudo pip install docker-compose
```

  - [OR] Install with download the latest version of Docker Compose:

```
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
    
  - **Test the installation**: `docker-compose --version`

## 1.3  Install project

```
git clone https://gitlab.com/loc.phan.connectiv/weather-pxl.git
cd weather-pxl
docker-compose build
docker-compose up -d
docker exec -it weather-server bash
cd weather.demo.vn/
composer install
chown -R $USER:www-data storage
chown -R $USER:www-data bootstrap/cache
chmod -R 775 bootstrap/cache
chmod -R 775 storage
php artisan key:generate
exit

```