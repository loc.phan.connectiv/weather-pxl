<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container" id="app">
        <sidebarleft-component></sidebarleft-component>
        <div class="container-content">
            <header-component></header-component>
            <content-component></content-component>
        </div>
    </div>
</body>
<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
</html>