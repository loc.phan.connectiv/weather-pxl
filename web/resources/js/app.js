/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
 
Vue.use(VueMaterial);

// require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('header-component', require('./components/layouts/HeaderComponent.vue').default);
Vue.component('sidebarleft-component', require('./components/layouts/SidebarleftComponent.vue').default);
Vue.component('content-component', require('./components/layouts/ContentComponent.vue').default);
Vue.component('dashboard-component', require('./components/contents/DashboardComponent.vue').default);
Vue.component('detail-component', require('./components/contents/DetailComponent.vue').default);
Vue.component('now-component', require('./components/contents/NowComponent.vue').default);
Vue.component('now-child-component', require('./components/contents/NowChildComponent.vue').default);
Vue.component('hour-component', require('./components/contents/HourComponent.vue').default);
Vue.component('hour-child-component', require('./components/contents/HourChildComponent.vue').default);
Vue.component('hour-title-component', require('./components/contents/HourTitleComponent.vue').default);
Vue.component('hour-detail-component', require('./components/contents/HourDetailComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
