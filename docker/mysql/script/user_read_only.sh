#!/usr/bin/env bash
GRANT SELECT ON `gflproject`.`user_value_scores` TO 'gfl-demo'@'%' IDENTIFIED BY 'Ha[55*W}';
GRANT SELECT ON `gflproject`.`spec_scores` TO 'gfl-demo'@'%' IDENTIFIED BY 'Ha[55*W}';
GRANT SELECT ON `gflproject`.`charts` TO 'gfl-demo'@'%' IDENTIFIED BY 'Ha[55*W}';
FLUSH PRIVILEGES;
